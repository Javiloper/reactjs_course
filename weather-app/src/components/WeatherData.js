import React, {Fragment} from 'react'
import WeatherExtraInfo from './WeatherExtraInfo'
import WeatherTemperature from './WeatherTemperature'

const WeatherData = () => (
    <Fragment>
        <WeatherTemperature temperature={25}/>
        <WeatherExtraInfo humidity={82} wind={23}/>
    </Fragment>
)

export default WeatherData