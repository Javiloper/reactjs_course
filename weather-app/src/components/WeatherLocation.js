import React, {Fragment} from 'react'
import Location from './Location';
import WeatherData from './WeatherData';


const WeatherLocation = () => {
    return (
        <Fragment>
            <Location city={'Valencia'}/>
            <WeatherData></WeatherData>
        </Fragment>
    )
}

export default WeatherLocation