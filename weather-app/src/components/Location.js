import React, {Fragment} from 'react'


const Location = (props) => {

    const city = props.city

    return (
        <Fragment>
            <h1>{city}</h1>
        </Fragment>
    )
}

export default Location