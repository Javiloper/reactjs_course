import React, {Fragment} from 'react'


const WeatherExtraInfo = ({humidity, wind}) => (
    <Fragment>
        <span>{`${humidity} %`}</span>
        <span>{`${wind} km/h`}</span>
    </Fragment>
)

export default WeatherExtraInfo